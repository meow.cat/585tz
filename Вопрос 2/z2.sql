SELECT
    category.id,
    category.content

FROM
    category

LEFT JOIN
    docs

ON
    docs.category = category.id

GROUP BY
    category.id

HAVING
    COUNT(*) > 1