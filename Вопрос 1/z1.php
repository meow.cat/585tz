<?php

include_once __DIR__.'/inc/serializer.php';
include_once __DIR__.'/inc/abstractClass.php';
include_once __DIR__.'/inc/dataMapper.php';

class ExampleClass extends AbstractClass
{
    private string $variable;
    private DataMapper $dataMapper;
    private ApiPlatform\Core\Serializer $serializer;

    public function getVariable(): string
    {
        return $this->variable;
    }

    public function setVariable(string $variable): self
    {
        $this->variable = $variable;
        return $this;
    }
    
    public function getDataMapper(): DataMapper
    {
        return $this->dataMapper;
    }
    
    public function setDataMapper(DataMapper $dataMapper): self
    {
        $this->dataMapper = $dataMapper;
        return $this;
    }
    
    public function getSerializer(): ApiPlatform\Core\Serializer
    {
        return $this->serializer;
    }
    
    public function setSerializer(ApiPlatform\Core\Serializer $serializer): self
    {
        $this->serializer = $serializer;
        return $this;
    }
}


$objTest = new ExampleClass();

$objTest->setVariable('test');
$objTest->setDataMapper(new DataMapper);
$objTest->setSerializer(new ApiPlatform\Core\Serializer);

var_dump($objTest->getVariable());
var_dump($objTest->getDataMapper());
var_dump($objTest->getSerializer());