# Вопрос 1. Провести код ревью.

### Дано:

```php
<?php

class ExampleClass extends AbstractClass
{
    private $variable = 1;
    private DataMapper $dataMapper;

    public function __construct(string $variable)
    {
        $this->variable = $variable;
        $this->dataMapper = new DataMapper();
        $this->serializer = new ApiPlatform\Core\Serializer\();
    }

    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * @return DataMapper
     */
    public function getDataMapper(): DataMapper
    {
        return $this->dataMapper;
    }

    /**
     * @param DataMapper $dataMapper
     * @return StrongExapmle
     */
    public function setDataMapper(DataMapper $dataMapper): StrongExapmle
    {
        $this->dataMapper = $dataMapper;
        return $this;
    }
}
```



# Вопрос 2. Написать запрос, чтобы вывести разделы, где содержатся больше 1 книги.

### Дано:

```sql
CREATE TABLE IF NOT EXISTS `docs` (
  `id` int(6) unsigned NOT NULL,
  `category` int(3) unsigned NOT NULL,
  `content` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;
INSERT INTO `docs` (`id`, `category`, `content`) VALUES
  ('1', '1', 'The earth is flat'),
  ('2', '1', 'One hundred angels can dance on the head of a pin'),
  ('3', '2', 'The earth is flat and rests on a bull\'s horn'),
  ('4', '3', 'The earth is like a ball.');


CREATE TABLE IF NOT EXISTS `category` (
  `id` int(6) unsigned NOT NULL,
  `content` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

INSERT INTO `category` (`id`, `content`) VALUES
  ('1', 'geografy'),
  ('2', 'filosofy'),
  ('3', 'earth');
```



# Вопрос 3. Задача вывести за каждый день с 2020-01-01 по 2020-01-10 стоимость всех товаров на складе (таблица products).

### Для проверки ( тут немного не точные цифры =) ):
    
За 2020-01-01, например, получается для товара (1) 50030 + для товара (2) 300010 + для товара (3) 25750 = 63750, а за 2020-01-03 - 31133 + 97017 + 20750 = 41753.

### Дано:

```sql
CREATE TABLE products (
  id INT NOT NULL AUTO_INCREMENT,
  date DATE,
  product_id INT NOT NULL,
  quantity INT NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO products
    (
      date,
      product_id,
      quantity
    )
VALUES 
    ("2020-01-01",1,500),
    ("2020-01-01",2,3000),
    ("2020-01-01",3,25),
    ("2020-01-02",1,450),
    ("2020-01-02",2,2500),
    ("2020-01-02",3,23),
    ("2020-01-03",1,311),
    ("2020-01-03",2,970),
    ("2020-01-03",3,20),
    ("2020-01-04",1,1000),
    ("2020-01-04",2,10000),
    ("2020-01-04",3,50),
    ("2020-01-05",1,632),
    ("2020-01-05",2,7868),
    ("2020-01-05",3,39),
    ("2020-01-06",1,632),
    ("2020-01-06",2,7868),
    ("2020-01-06",3,39),
    ("2020-01-07",1,737),
    ("2020-01-07",2,6999),
    ("2020-01-07",3,37),
    ("2020-01-08",1,590),
    ("2020-01-08",2,4500),
    ("2020-01-08",3,29),
    ("2020-01-09",1,311),
    ("2020-01-09",2,970),
    ("2020-01-09",3,20),
    ("2020-01-10",1,1000),
    ("2020-01-10",2,10000),
    ("2020-01-10",3,50);

CREATE TABLE price_log (
  id INT NOT NULL AUTO_INCREMENT,
  date DATE,
  product_id INT NOT NULL,
  price INT NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO price_log
    (
      date,
      product_id,
      price
    )
VALUES 
    ("2019-12-29",1,30),
    ("2019-11-01",2,10),
    ("2019-11-02",2,11),
    ("2019-01-01",3,750),
    ("2020-01-03",1,33),
    ("2020-01-04",1,32),
    ("2020-01-02",2,17),
    ("2020-01-09",2,20),
    ("2020-01-05",3,699);
```

