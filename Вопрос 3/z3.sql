SELECT
    prod_res.prod_date,
    SUM(prod_res.prod_sum)

FROM
    (
        SELECT
            products.date AS prod_date,
            (products.quantity * price_log.price) AS prod_sum

        FROM
            products

        LEFT JOIN
            price_log

        ON
            price_log.product_id = products.product_id

        WHERE
            price_log.date =
            (
                SELECT
                    max(price_log_sub.date)
                FROM
                    price_log AS price_log_sub
                WHERE
                    price_log_sub.product_id = products.product_id
                    AND price_log_sub.date <= products.date 
            )

    ) AS prod_res

GROUP BY
    prod_res.prod_date

ORDER BY
    prod_res.prod_date ASC